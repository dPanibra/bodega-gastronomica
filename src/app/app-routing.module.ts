import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/home/home.module').then((m) => m.HomeModule),
      },
    ],
  },
  {
    path: 'categorias',
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/categorias/categorias.module').then((m) => m.CategoriasModule),
      },
    ],
  },
  // { path: '**', redirectTo: 'battle' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
