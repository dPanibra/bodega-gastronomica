import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioHomeComponent } from './formulario-home.component';

describe('FormularioHomeComponent', () => {
  let component: FormularioHomeComponent;
  let fixture: ComponentFixture<FormularioHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
