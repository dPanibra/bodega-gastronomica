import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulario-home',
  templateUrl: './formulario-home.component.html',
  styleUrls: ['./formulario-home.component.scss'],
})
export class FormularioHomeComponent implements OnInit {
  title = 'My first AGM project';
  lat = -16.39889;
  lng = -71.535;
  constructor(private router: Router) {}

  ngOnInit(): void {}

  onMapReady(map) {
    map.addListener('click', (e) => {
      this.lat = e.latLng.lat();
      this.lng = e.latLng.lng();
      console.log(e.latLng.lat());
    });
  }
  btnClick= function () {
    this.router.navigateByUrl('/categorias');
};
}
