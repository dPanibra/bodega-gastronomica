import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule} from './../../material.module'

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HeaderHomeComponent } from './header-home/header-home.component';
import { CatalogoHomeComponent } from './catalogo-home/catalogo-home.component';
import { FormularioHomeComponent } from './formulario-home/formulario-home.component';
import { FooterHomeComponent } from './footer-home/footer-home.component';

@NgModule({
  declarations: [
    HomeComponent,
    HeaderHomeComponent,
    CatalogoHomeComponent,
    FormularioHomeComponent,
    FooterHomeComponent,
  ],
  imports: [CommonModule, HomeRoutingModule, MaterialModule],
})
export class HomeModule {}
