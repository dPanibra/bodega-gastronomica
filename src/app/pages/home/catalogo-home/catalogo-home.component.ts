import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Observable, Observer } from 'rxjs';
export interface Producto {
  nombre: string;
  detalle: string;
  precio: number;
  unidad: string;
  img: string;
  cantidad: number;
  total: number;
}
export interface ExampleTab {
  label: string;
  content: Array<Producto>;
}
@Component({
  selector: 'app-catalogo-home',
  templateUrl: './catalogo-home.component.html',
  styleUrls: ['./catalogo-home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CatalogoHomeComponent implements OnInit {
  asyncTabs: Observable<ExampleTab[]>;

  constructor() {
    this.asyncTabs = new Observable((observer: Observer<ExampleTab[]>) => {
      setTimeout(() => {
        observer.next([
          {
            label: 'MARINOS Y CRIOLLOS',
            content: [
              {
                nombre: 'Adobo ',
                detalle: 'Adobo con su rocotito ',
                precio: 24,
                img: 'http://aldeliapi.david.almaquinto.com/images/ADOBO.png',
                cantidad: 0,
                total: 0,
                unidad: 'Plato',
              },
              {
                nombre: 'Lomo saltado ',
                detalle: '',
                precio: 48,
                img: 'http://aldeliapi.david.almaquinto.com/images/lomo.png',
                cantidad: 0,
                total: 0,
                unidad: 'Plato',
              },
              {
                nombre: 'Arroz de mariscos ',
                detalle: 'Langostinos, lapas, conchas',
                precio: 38.5,
                img: 'http://aldeliapi.david.almaquinto.com/images/arroz.png',
                cantidad: 0,
                total: 0,
                unidad: 'Plato',
              },
              {
                nombre: 'Ceviche de pesca blanca ',
                detalle:
                  'Leche de tigre clásica de rocoto, con camote, maiz grillado y canchita, cebollita',
                precio: 34,
                img:
                  'http://aldeliapi.david.almaquinto.com/images/ceviche%20clasico.png',
                cantidad: 0,
                total: 0,
                unidad: 'Plato',
              },
              {
                nombre: 'Ceviche de corvina ',
                detalle:
                  'Leche de tigre clásica de rocoto, con camote, maiz grillado y canchita, cebollita',
                precio: 49,
                img:
                  'http://aldeliapi.david.almaquinto.com/images/ceviche%20corvina.png',
                cantidad: 0,
                total: 0,
                unidad: 'Plato',
              },
              {
                nombre: 'Pulpo al Olivo ',
                detalle: 'Crema de aceitunas y papas',
                precio: 33,
                img:
                  'http://aldeliapi.david.almaquinto.com/images/pulpo%20al%20olivo.png',
                cantidad: 0,
                total: 0,
                unidad: 'Plato',
              },
              {
                nombre: 'Pulpo a la parrilla ',
                detalle: 'En salsa anticuchera, con chimichurri y papas',
                precio: 33,
                img:
                  'http://aldeliapi.david.almaquinto.com/images/pulpo%20anticuchero.png',
                cantidad: 0,
                total: 0,
                unidad: 'Plato',
              },
              {
                nombre: 'Chupe de camarones ',
                detalle: 'Bajo pedido',
                precio: 45,
                img: 'http://aldeliapi.david.almaquinto.com/images/chupe.png',
                cantidad: 0,
                total: 0,
                unidad: 'Plato',
              },
              {
                nombre: 'Seco de Cordero ',
                detalle: '',
                precio: 38,
                img: 'http://aldeliapi.david.almaquinto.com/images/seco.png',
                cantidad: 0,
                total: 0,
                unidad: 'Plato',
              },
            ],
          },
          {
            label: 'ESPECIALIDAD DEL CHEF',
            content: [
              {
                nombre: 'Hamburguesa de camarones',
                detalle:
                  'Colas de camaron, omegas de pescado, ajo y cebolla confitadas, oregano',
                precio: 24.5,
                img:
                  'http://aldeliapi.david.almaquinto.com/images/hamburguesa.png',
                cantidad: 0,
                total: 0,
                unidad: '150 gr',
              },
              {
                nombre: 'Trucha ahumada',
                detalle: 'Trucha rosada ahumada en aceite de oliva y eneldo ',
                precio: 16,
                img: 'http://aldeliapi.david.almaquinto.com/images/trucha.png',
                cantidad: 0,
                total: 0,
                unidad: '150 gr',
              },
            ],
          },
          {
            label: 'POSTRES',
            content: [
              {
                nombre: 'Queso de helado artesanal',
                detalle: '',
                precio: 12,
                img:
                  'http://aldeliapi.david.almaquinto.com/images/QUESO%20HEALDO.png',
                cantidad: 0,
                total: 0,
                unidad: '1/2 Lt.',
              },
              {
                nombre: 'Pack de trufas de chocolate ',
                detalle:
                  '4 sabores: Clasica de chocolate /Arándanos y pisco /Café  /Whisky',
                precio: 15,
                img: 'http://aldeliapi.david.almaquinto.com/images/TRUFAS.png',
                cantidad: 0,
                total: 0,
                unidad: '7 Unidades',
              },
            ],
          },
          {
            label: 'EL BAR',
            content: [
              {
                nombre: 'General',
                detalle: '',
                precio: 1,
                img: 'https://toronto.hispanocity.com/wp-content/uploads/2019/05/causa-rellena-to.jpg',
                unidad: 'asd',
                cantidad: 0,
                total: 0,
              },
            ],
          },
          {
            label: 'OTROS',
            content: [
              {
                nombre: 'General',
                detalle: '',
                precio: 1,
                img: 'https://toronto.hispanocity.com/wp-content/uploads/2019/05/causa-rellena-to.jpg',
                unidad: 'asd',
                cantidad: 0,
                total: 0,
              },
            ],
          },
        ]);
      }, 1500);
    });
  }

  ngOnInit(): void {}

  add(producto: Producto, a) {
    if (a == 'menos' && producto.cantidad <= 0) {
      producto.cantidad = 0;
      producto.total=0;
    } else if (a == 'menos') {
      producto.cantidad--;
      producto.total=producto.cantidad*producto.precio;
    } else if (a == 'mas') {
      producto.cantidad++;
      producto.total=producto.cantidad*producto.precio;

    }
  }
}
