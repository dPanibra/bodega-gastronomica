import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoHomeComponent } from './catalogo-home.component';

describe('CatalogoHomeComponent', () => {
  let component: CatalogoHomeComponent;
  let fixture: ComponentFixture<CatalogoHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
